package abesit.in.bigata;

import org.apache.zookeeper.ZooKeeper;



import org.apache.zookeeper.KeeperException;

import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        CRUD_ZK zk = new CRUD_ZK();



        // znode path
        String znode_path = "/firstZnodeDatreon"; // Assign path to znode


        // data in byte array
        byte[] data = "zookeeper znode data".getBytes();
        // Declare data to be update


        byte[] update_data = "updated data".getBytes();
        zk.intialize();
        try {
            zk.create (znode_path ,data);


            System.out.println("znode path : "+ znode_path + "data : " + data);

            zk.getZNodeStats(znode_path );

            zk.update(znode_path,update_data);

            System.out.println("update data" +update_data);

            zk.delete(znode_path);
            zk.close();
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
